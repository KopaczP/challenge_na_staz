﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace ChallengeNaStaz.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "Nazwa użytkownika")]
        public override string UserName { get; set; }

        //relacja 1 Dietetyk do n Pacjentów
        [Display(Name = "Pacjenci")]
        public virtual ICollection<Patient> Patients { get; set; }
    }
}
