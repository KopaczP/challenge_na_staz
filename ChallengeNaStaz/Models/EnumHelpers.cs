﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public static class EnumHelpers
    {
        public static string GetDisplayName (this Enum enumeration)
        {
            return enumeration.GetType().GetMember(enumeration.ToString())
                   .First()
                   .GetCustomAttribute<DisplayAttribute>()
                   .Name;
        }

    }
}
