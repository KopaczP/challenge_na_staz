﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public class DietRaw
    {
        [Display(Name = "Nazwa diety")]
        public string DietName { get; set; }
        [Display(Name = "Posiłki")]
        public virtual ICollection<int> Meals { get; set; }
        [Display(Name = "Idealna dla")]
        public string IdealFor { get; set; }
    }
}
