﻿using ChallengeNaStaz.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public class Meal
    {
        public int MealId { get; set; }

        //Nazwa posiłku
        [Display(Name = "Nazwa posiłku")]
        public string MealName { get; set; }

        //Przepis (pole opisowe)
        [Display(Name = "Przepis")]
        public string Recipe { get; set; }

        //Lista zakupów (pole opisowe)
        [Display(Name = "Lista zakupów")]
        public string ShoppingList { get; set; }

        [Display(Name = "Typ posiłku")]
        public MealTypeEnum MealType { get; set; }

        /* RELACJE */
        /*
        //Relacja Dieta 1 do n Posiłków
        public Diet Diet { get; set; } //nav property
        public int? DietId { get; set; } //może ale nie musi byc przypisany do diety*/

        //relacja n Diet do n Posiłków
        [Display(Name = "Przypisany do diet")]
        public virtual ICollection<DietMeal> DietMeals { get; set; }
    }
}
