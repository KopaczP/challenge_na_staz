﻿using ChallengeNaStaz.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public class Alert
    {
        public AlertType Type { get; set; }
        public bool Dismissible { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
