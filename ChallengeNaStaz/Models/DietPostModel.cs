﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public class DietPostModel
    {
        //public int dietid { get; set; }
        public string Name { get; set; }
        public virtual ICollection<int> MealIds { get; set; }
    }
}
