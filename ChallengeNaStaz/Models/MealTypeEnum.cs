﻿using System.ComponentModel.DataAnnotations;

namespace ChallengeNaStaz.Models
{
    public enum MealTypeEnum
    {
        [Display (Name = "Śniadanie")]
        Breakfast,

        [Display (Name = "Drugie śniadanie")]
        SecondBreakfast,

        [Display (Name = "Obiad")]
        Dinner,

        [Display (Name = "Podwieczorek")]
        Lunch,

        [Display (Name = "Kolacja")]
        Supper
    }
}