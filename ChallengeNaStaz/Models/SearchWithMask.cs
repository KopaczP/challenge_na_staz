﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T">Specyfikuje typ elementu kolekcji, najczęściej jest to int, ale niekiedy musi być to GUID</typeparam>

    public class SearchWithMask<T>
    {
        public string QueryString { get; set; }
        public virtual ICollection<T> ExcludeIds { get; set; }
    }
    public class SearchWithMask : SearchWithMask<int>
    {
        public new virtual ICollection<int> ExcludeIds { get; set; }
    }
}
