﻿using ChallengeNaStaz.Models.UserViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public class Diet
    {
        [Key]
        public int DietID { get; set; }

        //Nazwa diety
        [Display(Name = "Nazwa diety")]
        public string DietName { get; set; }

        //Kiedy dodano
        [Display(Name = "Kiedy dodano")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        public DateTime DateAdded { get; set; }

        [NotMapped]
        public string DateAddedString
        {
            get
            {
                return DateAdded.ToString("dd.MM.yyyy");
            }
        }

        //Ile posiłków ma ta dieta
        [NotMapped]
        [Display(Name = "Ile posiłków ma ta dieta")]
        public int HowManyMeals
        {
            get
            {
                if (DietMeals != null)
                {
                    return DietMeals.Where(dm => dm.DietId == this.DietID).ToList().Count;
                }
                return 0;
            }
        }

        //Ile pacjentów jest na tej diecie
        [NotMapped]
        [Display(Name = "Ilu pacjentów ma ta dieta")]
        public int AmountOfPatientsOnThisDiet
        {
            get
            {
                if (Patients != null)
                {
                    return Patients.Count;
                }
                return 0;
            }
        }

        [Display(Name = "Dieta idealna dla osób")]
        public string IdealForPatients { get; set; }

        /* RELACJE */

        //relacja 1 Dieta do n Pacjentów
        [Display(Name = "Pacjenci na tej diecie")]
        public List<Patient> Patients { get; set; } //Nav property
        //public int? PatientId { get; set; } //może ale nie musi być przypsiana do pacjenta

        /*
        //relacja 1 Dieta do n Posiłków
        //Lista posiłków na tej diecie
        public virtual ICollection<Meal> Meals { get; set; }
        */

        //relacja n Diet do n Posiłków
        [Display(Name = "Posiłki na tej diecie")]
        public virtual ICollection<DietMeal> DietMeals { get; set; }

        public Diet () {
            DateAdded = DateTime.Now;
        }
    }
}
