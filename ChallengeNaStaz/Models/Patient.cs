﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public class Patient
    {
        public int PatientId { get; set; }

        //Imie 
        [Display(Name = "Imie")]
        public string Name { get; set; }

        //Nazwisko
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        //Kiedy dodano
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "dd.MM.yyyy")]
        [Display(Name = "Data dodania")]
        public DateTime DateAdded { get; set; }
        [NotMapped]
        public string DateAddedString {
            get
            {
                return DateAdded.ToString("dd.MM.yyyy");
            }
        }

        //Długość trwania diety, może być to DateOffset 
        [Display(Name = "Czas trwania diety")]
        public int DietDuration { get; set; }

        //Ilość odbytych konsultacji
        [Display(Name = "Odbyte konsultacje")]
        public int DoneConsultations { get; set; }

        //Planowana waga na koniec diety
        [Display(Name = "Waga docelowa")]
        public int PlannedWeight { get; set; }

        //Co lubi
        [Display(Name = "Lubi")]
        public string WhatPatientLikes { get; set; }

        //Co nie lubi
        [Display(Name = "Nie lubi")]
        public string WhatPatientDoesntLike { get; set; }

        /* RELACJE */

        //relacja 1 Dieta do n Pacjentów
        public Diet Diet { get; set; }
        public int? DietId { get; set; } //może ale nie musi być przypisany do diety
        
        //relacja 1 Pacjent do 1 KartyPacjenta
        public PatientsCard PatitentsCard { get; set; }

        //relacja 1 Dietetyk do n Pacjentów
        //[ForeignKey("ApplicationUser")]
        public string NutritionistId { get; set; }
        public ApplicationUser Nutritionist { get; set; }

        public Patient()
        {
            DietDuration = 60;
            DoneConsultations = 0;
            PlannedWeight = 75;
            WhatPatientLikes = "Jabłko";
            WhatPatientDoesntLike = "Mleko";
            DateAdded = DateTime.UtcNow;
        }
    }
}
