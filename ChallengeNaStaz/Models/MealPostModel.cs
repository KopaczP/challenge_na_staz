﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public class MealPostModel
    {
        public int Id { get; set; }
        public string Recipe { get; set; }
        public string ShoppingList { get; set; }
    }
}
