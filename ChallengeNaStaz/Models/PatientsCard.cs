﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models
{
    public class PatientsCard
    {
        public int PatientsCardId { get; set; }

        //Waga przy pierwszej wizycie
        [Display(Name = "Waga poczatkowa")]
        public float WeightAtFirstConsultation { get; set; }

        //Wiek
        [Display(Name = "Wiek")]
        public int Age { get; set; }

        //tryb życia
        [Display(Name = "Tryb życia")]
        public string LivingStyle { get; set; }

        //Wzrost
        [Display(Name = "Wzrost")]
        public float Height { get; set; }

        //Liczba wizyt w poradni
        [Display(Name = "Odbyte konsultacje")]
        public int DoneConsulations { get; set; }

        /* RELACJE */

        //relacja 1 Pacjent do 1 KartyPacjenta
        public Patient Patient { get; set; }
        public int PatientId { get; set; } //nie może istnieć bez Pacjenta
    }
}
