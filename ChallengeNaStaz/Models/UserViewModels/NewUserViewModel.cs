﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Models.UserViewModels
{
    public class NewUserViewModel
    {
        [Display(Name = "Nazwa użytkownika")]
        public string UserName { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Hasło")]
        public string Password { get; set; }
    }
}
