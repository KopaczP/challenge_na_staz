﻿using ChallengeNaStaz.Models;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Extensions
{
    public static class AlertHelper
    {
        //używane by uniknąć konfliktów przy dodawaniu danych do tempdata
        public const string UniqueIdentifier = "alert123";
        private static Random rand = new Random();

        public static void AddAlert(this ITempDataDictionary tempdata, AlertType alertType, string message, string title = null, bool dismissible = true)
        {
            Alert alert = new Alert()
            {
                Type = alertType,
                Message = message,
                Title = title,
                Dismissible = dismissible
            };
            tempdata.Add(UniqueIdentifier + rand.Next(1000), JsonConvert.SerializeObject(alert));
        }

        public static IList<Alert> GetAlerts (this ITempDataDictionary tempData)
        {
            var alerts = tempData.Where(td => td.Key.Contains(UniqueIdentifier)).ToList();
            IList<Alert> deserializedAlerts = new List<Alert>();

            foreach (var alert in alerts)
            {
                deserializedAlerts.Add(JsonConvert.DeserializeObject<Alert>((string)alert.Value));
            }
            return deserializedAlerts;
        }
    }

    public enum AlertType
    {
        Success = 0,
        Danger = 1,
        Warning = 2,
        Info = 3
    }
}
