﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using ChallengeNaStaz.Models.UserViewModels;
using ChallengeNaStaz.Models;

namespace ChallengeNaStaz.Data
{
    public class DBSEED
    {
        /*
        ApplicationDbContext context;
        DBSEED(ApplicationDbContext context)
        {
            this.context = context;
        }*/
        public static async Task Seed(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            Models.Meal[] meals = new Models.Meal[]
            {
                new Models.Meal {MealName = "Leczo", Recipe = "Gotuj 5 minu", ShoppingList = "Pomidory 1kg, Papryki 3 kolory", MealType = MealTypeEnum.Breakfast},
                new Models.Meal {MealName = "Rosół", Recipe = "Gotuj 50 minut", ShoppingList = "Cziken, kluski, włoszczyna", MealType = MealTypeEnum.Supper},
                new Models.Meal {MealName = "Sałatka", Recipe = "Obierz wiele przypadkowych warzyw", ShoppingList = "Co tylko chcesz", MealType = MealTypeEnum.Dinner},
            };

            Models.Diet[] diets = new Models.Diet[]
            {
                new Models.Diet {DietName = "Cud", DateAdded = DateTime.Now},
                new Models.Diet {DietName = "Cud1", DateAdded = DateTime.Now},
                new Models.Diet {DietName = "Cud2", DateAdded = DateTime.Now}
            };

            Models.Patient[] patients = new Models.Patient[]
            {
                new Models.Patient {Name = "Patryk", DateAdded = DateTime.Now, DietDuration = 60, DoneConsultations = 0, LastName = "Kopacz", PlannedWeight = 76, WhatPatientDoesntLike = "Banany", WhatPatientLikes = "Obwarzanki" },
                new Models.Patient {Name = "Kamil", DateAdded = DateTime.Now, DietDuration = 61, DoneConsultations = 1, LastName = "Klyta", PlannedWeight = 80, WhatPatientDoesntLike = "Jabłka", WhatPatientLikes = "Banany" },
                new Models.Patient {Name = "Michał", DateAdded = DateTime.Now, DietDuration = 62, DoneConsultations = 2, LastName = "Gołda", PlannedWeight = 76, WhatPatientDoesntLike = "Orzechy", WhatPatientLikes = "Pomidory" }
            };

            Models.PatientsCard[] patientsCards = new Models.PatientsCard[]
            {
                new Models.PatientsCard {WeightAtFirstConsultation = 120, Age = 17, DoneConsulations = 1, Height = 170, LivingStyle = "Siedzący", Patient = patients[0]},
                new Models.PatientsCard {WeightAtFirstConsultation = 110, Age = 18, DoneConsulations = 2, Height = 180, LivingStyle = "Stojący", Patient = patients[1]},
                new Models.PatientsCard {WeightAtFirstConsultation = 100, Age = 19, DoneConsulations = 3, Height = 190, LivingStyle = "Leżący", Patient = patients[2]}
            };
            IdentityRole[] identityRoles = new IdentityRole[]
            {
                new IdentityRole {Name = "Administrator"},
                new IdentityRole {Name = "Nutritionist"},
                new IdentityRole {Name = "Patient"},
            };

            string[] roles = new string[]
            {
                "Administrator",
                "Nutritionist",
                "Patient"
            };

            if (!context.Meals.Any())
            {
                context.Meals.AddRange(meals);
            }
            await context.SaveChangesAsync();
            
            if (!context.Patients.Any())
            {
                context.Patients.AddRange(patients);
            }
            await context.SaveChangesAsync();

            if (!context.Diets.Any())
            {
                context.Diets.AddRange(diets);
            }
            await context.SaveChangesAsync();

            if (!context.DietMeals.Any())
            {
                //relacja diet i posiłków
                var qdiets = context.Diets.ToList();
                var qMeals = context.Meals.ToList();
                List<DietMeal> joinTable = new List<DietMeal>();
                
                for (int i = 0; i < qdiets.Count; i++)
                {
                    joinTable.Add(new DietMeal { DietId = qdiets[i].DietID, MealId = qMeals[i].MealId });
                }
                joinTable.Add(new DietMeal { DietId = 1, MealId = qMeals[2].MealId }); // jeden posiłek należy do 2 diet

                context.DietMeals.AddRange(joinTable);
                await context.SaveChangesAsync();
            }

            //Wymaga zaseedowanej tabeli pacjenta ze względu na relacje 1 Pacjent do 1 KartyPacjenta
            if (!context.PatientCards.Any())
            {
                var tpatients = context.Patients.ToList();
                int c = 0;
                foreach (var card in patientsCards)
                {
                    card.Patient = tpatients[c];
                    c++;
                }
                context.PatientCards.AddRange(patientsCards);
            }
            await context.SaveChangesAsync();
            
            foreach (var roleName in roles)
            {
                if (!context.Roles.Any(r => r.Name == roleName))
                {
                    context.Roles.Add(new IdentityRole
                    {
                        Name = roleName,
                        NormalizedName = roleName.ToUpper()
                    });
                }
            }
            await context.SaveChangesAsync();
        }
    }
}
