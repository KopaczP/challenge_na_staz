﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ChallengeNaStaz.Models;
using ChallengeNaStaz.Models.UserViewModels;

namespace ChallengeNaStaz.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Meal> Meals { get; set; }
        public DbSet<Diet> Diets { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientsCard> PatientCards { get; set; }
        public DbSet<DietMeal> DietMeals { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            //pacjent 1 do diety 0..1
            /*builder.Entity<Patient>()
                .HasOne(p => p.Diet)
                .WithOne(d => d.Patient)
                .HasForeignKey<Diet>(d => d.PatientId)
                .OnDelete(DeleteBehavior.SetNull);*/

            //relacja 1 Dieta do n Pacjentów
            builder.Entity<Diet>()
                .HasMany(d => d.Patients)
                .WithOne(p => p.Diet)
                .HasForeignKey(p => p.DietId)
                .OnDelete(DeleteBehavior.SetNull);

            //relacja n Diet do n Posiłków
            /*builder.Entity<Diet>()
                .HasMany(d => d.Meals)
                .WithOne(m => m.Diet)
                .OnDelete(DeleteBehavior.SetNull);*/

            //relacja n Diet do n Posiłków

            builder.Entity<DietMeal>()
                .HasKey(dm => new { dm.DietId, dm.MealId });

            builder.Entity<DietMeal>()
                .HasOne(dm => dm.Diet)
                .WithMany(d => d.DietMeals)
                .HasForeignKey(dm => dm.DietId);

            builder.Entity<DietMeal>()
                .HasOne(dm => dm.Meal)
                .WithMany(d => d.DietMeals)
                .HasForeignKey(dm => dm.MealId);

            //relacja 1 Pacjent do 1 KartyPacjenta
            builder.Entity<Patient>()
                .HasOne(p => p.PatitentsCard)
                .WithOne(c => c.Patient);

            //relacja 1 Dietetyk do n Pacjentów
            builder.Entity<ApplicationUser>()
                .HasMany(d => d.Patients)
                .WithOne(p => p.Nutritionist);
        }
    }
}
