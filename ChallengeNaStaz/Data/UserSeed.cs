﻿using ChallengeNaStaz.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace ChallengeNaStaz.Data
{
    public class UserSeed
    {        
        /*
        UserSeed (IServiceProvider serviceProvider)
        {
            userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
        }
        */

        public static async Task SeedUsers(IServiceProvider serviceProvider)
        {
            string email = "Kopaczkapl@gmail.com";
            string password = "zaq1@WSX";

            UserManager<ApplicationUser> userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            if (!userManager.Users.Any())
            {
                await userManager.CreateAsync(new ApplicationUser
                {
                    Email = email,
                    UserName = email,
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                    password
                );
                var admin = userManager.FindByEmailAsync(email);
                await userManager.AddToRoleAsync(admin.Result, "Administrator");
            }
        }
    }
}
