﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ChallengeNaStaz.Data;
using ChallengeNaStaz.Models;
using Microsoft.AspNetCore.Identity;
using ChallengeNaStaz.Models.UserViewModels;

namespace ChallengeNaStaz.Controllers
{
    //todo dodać alerty
    //todo dodać zarzadzanie
    public class UsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _context;

        public UsersController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,  ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            return View(await _userManager.GetUsersInRoleAsync("Nutritionist"));
        }

        // GET: Users/Create
        //Zakładanie konta dietetyka
        public IActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(NewUserViewModel applicationUser)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser {
                    UserName = applicationUser.Email,
                    Email = applicationUser.Email,
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                //posypie sie jeśli hasło bedzie za słabe
                await _userManager.CreateAsync(user, applicationUser.Password);
                var retrievedUser = await _userManager.FindByNameAsync(user.UserName);

                await _userManager.AddToRoleAsync(retrievedUser, "Nutritionist");

                //await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(applicationUser);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = _userManager.FindByIdAsync(id);

            await _userManager.DeleteAsync(user.Result);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(string id)
        {
            return _userManager.FindByIdAsync(id) != null;
        }
    }
}
