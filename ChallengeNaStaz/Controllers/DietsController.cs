﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ChallengeNaStaz.Data;
using ChallengeNaStaz.Models;
using ChallengeNaStaz.Extensions;

namespace ChallengeNaStaz.Controllers
{
    //[Route("[controller]/[action]")]
    public class DietsController : Controller
    {
        private readonly ApplicationDbContext context;

        public DietsController(ApplicationDbContext context)
        {
            this.context = context;
        }

        //webapi - uzyte w widoku Diets/Index
        [HttpGet]
        public IActionResult GetDiets(string queryString)
        {
            Console.WriteLine(queryString);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Diet> diets = new List<Diet>();
            if (!String.IsNullOrEmpty(queryString))
            {
                diets = context.Diets.Where(d => d.DietName.Contains(queryString)).Include(d => d.DietMeals).Include(d => d.Patients).ToList();
            }
            else
            {
                diets = context.Diets.Include(d => d.DietMeals).Include(d => d.Patients).ToList();
            }
            var json = Json(diets);
            return Json(diets);
        }

        //webapi - użyte w widoku Diets/Create
        ///todo - dodać sprawdzanie błędów, teraz metoda zawsze zróci kod ok
        /*[HttpPost]
        public IActionResult PostDiet([FromBody]DietPostModel diet)
        {
            //tworzy diete
            Diet d = new Diet();
            d.IdealForPatients = "TestKontrolera";
            d.DietName = diet.Name;

            //zapisuje
            context.Diets.Add(d);
            context.SaveChanges();

            //pobiera tą diete tak by miała id
            var retrievedDiet = context.Diets.Where(qdiet => qdiet.DietName == d.DietName).First();

            //tworzy join entity który złączy istniejącą diete z istniejącymi posiłkami
            List<DietMeal> join = new List<DietMeal>(diet.MealIds.Count);
            foreach (var mealID in diet.MealIds)
            {
                join.Add(new DietMeal { DietId = retrievedDiet.DietID, MealId = mealID });
            }

            //dodaje zapisuje
            context.DietMeals.AddRange(join);
            context.SaveChanges();

            return Ok();
        }*/

        // GET: Diets
        public async Task<IActionResult> Index()
        {
            /*if (TempData.ContainsKey("MSG"))
            {
                TempData.Add("MSG", new Tuple<int, string>(1, "Pomyślnie usunięto dietę"));
            }*/

            var list = await context.Diets
                .Include(d => d.DietMeals)
                .Include(d => d.Patients).ToListAsync();
            return View(list);
        }

        // GET: Diets/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Diets1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(DietRaw rawDiet) //wzorcowy
        {
            if (ModelState.IsValid)
            {
                Diet diet = new Diet
                {
                    DietName = rawDiet.DietName
                };
                context.Diets.Add(diet);
                await context.SaveChangesAsync();

                List<DietMeal> relation = new List<DietMeal>();
                for (int i = 0; i < rawDiet.Meals.Count; i++)
                {
                    relation.Add(new DietMeal()
                    {
                        DietId = diet.DietID,
                        MealId = rawDiet.Meals.ElementAt(i)
                    });
                }
                diet.IdealForPatients = rawDiet.IdealFor;
                diet.DietMeals = relation;

                await context.SaveChangesAsync();
                TempData.AddAlert(AlertType.Success, "Poprawnie utworzono dietę");
            }
            return RedirectToAction(nameof(Index));
            //return View(diet);
        }

        // GET: Diets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var diet = await context.Diets.Where(m => m.DietID == id).Include(m => m.DietMeals).FirstAsync();
            var diet = await context.Diets
                .Where(m => m.DietID == id)
                .Include(d => d.DietMeals)
                    .ThenInclude(dm => dm.Meal)
                .FirstAsync();
            if (diet == null)
            {
                return NotFound();
            }
            return View(diet);
        }

        // POST: Diets/Manage/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, DietRaw diet)
        {
            var td = await context.Diets.Where(dd => dd.DietID == id).Include(d => d.DietMeals).ThenInclude(dm => dm.Meal).FirstAsync();
            if (td == null)
            {
                return NotFound();
            }
            td.DietMeals.Clear();
            //gdy nie zapiszemy tutaj posypie sie entity tracking, coś z duplikowaniem entity
            await context.SaveChangesAsync();

            td.IdealForPatients = diet.IdealFor;
            td.DietName = diet.DietName;
            foreach (var mealID in diet.Meals)
            {
                td.DietMeals.Add(new DietMeal()
                {
                    DietId = id,
                    MealId = mealID,
                });
            }


            if (ModelState.IsValid)
            {
                try
                {
                    await context.SaveChangesAsync();
                    TempData.AddAlert(AlertType.Success, "Poprawnie zaktualizowano dietę");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (context.Diets.Where(d => d.DietID == id).First() != null)
                    {
                        TempData.AddAlert(AlertType.Warning, "Próba edycji nieistniejącej diety!", "BŁĄD");
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            return View(diet);
        }

        public IActionResult ConfirmDelete(int id)
        {
            var o = context.Diets.SingleOrDefault(d => d.DietID == id);
            context.Remove(o);
            context.SaveChanges();
            TempData.AddAlert(AlertType.Success, "Usunięto"/*, "Usuwanie diety"*/);
            return RedirectToAction(nameof(Index));
        }
    }
}
