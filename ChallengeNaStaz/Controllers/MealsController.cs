﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ChallengeNaStaz.Data;
using ChallengeNaStaz.Models;

namespace ChallengeNaStaz.Controllers
{
    //todo dodać alerty
    //todo dodać zarzadzanie
    //[Route("[controller]/[action]")]
    public class MealsController : Controller
    {
        private readonly ApplicationDbContext _context;

        //webapi - użyte w widoku Meals/Index
        //[HttpGet]
        public async Task<IActionResult> GetMeals([FromQuery] string queryString)
        {
            Console.WriteLine(queryString);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Meal> meals = new List<Meal>();
            if (!String.IsNullOrEmpty(queryString))
            {
                meals = await _context.Meals.Where(m => m.MealName.Contains(queryString)).ToListAsync();
            }
            else
            {
                meals = await _context.Meals.ToListAsync();
            }

            return Ok(meals);
        }

        //webapi
        /*[HttpGet("{id}")]
        public async Task<IActionResult> GetMeal([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var meal = await _context.Meals.SingleOrDefaultAsync(m => m.MealId == id);

            if (meal == null)
            {
                return NotFound();
            }

            return Ok(meal);
        }
        */
        //uzyte do pobrania view component z listą posiłków
        [HttpPost]
        public ActionResult FindMeals([FromBody]SearchWithMask query)
        {
            return ViewComponent("MealList", query);
        }

        //webapi - aktualizuje posiłek, użyte w widoku Diet/Create
        [HttpPut]
        public async Task<IActionResult> PutMeal([FromBody]MealPostModel updatedMeal)
        {
            //Meal temp = new Meal { MealId = mealId, Recipe = recipe, ShoppingList = shoppingList };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Meal meal = _context.Meals.Where(m => m.MealId == updatedMeal.Id).First();
            meal.Recipe = updatedMeal.Recipe;
            meal.ShoppingList = updatedMeal.ShoppingList;

            //_context.Entry(temp).State = EntityState.Modified;
            _context.Meals.Update(meal);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MealExists(meal.MealId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        public MealsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Meals1
        public async Task<IActionResult> Index()
        {
            return View(await _context.Meals.ToListAsync());
        }

        // GET: Meals1/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meal = await _context.Meals
                .SingleOrDefaultAsync(m => m.MealId == id);
            if (meal == null)
            {
                return NotFound();
            }

            return View(meal);
        }

        // GET: Meals1/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Meals1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MealId,MealName,Recipe,ShoppingList")] Meal meal)
        {
            if (ModelState.IsValid)
            {
                _context.Add(meal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(meal);
        }

        // GET: Meals1/Edit/5
        //[Route("{id?}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meal = await _context.Meals.SingleOrDefaultAsync(m => m.MealId == id);
            if (meal == null)
            {
                return NotFound();
            }
            return View(meal);
        }

        // POST: Meals1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MealId,MealName,Recipe,ShoppingList")] Meal meal)
        {
            if (id != meal.MealId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(meal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MealExists(meal.MealId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(meal);
        }

        // GET: Meals1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meal = await _context.Meals
                .SingleOrDefaultAsync(m => m.MealId == id);
            if (meal == null)
            {
                return NotFound();
            }

            return View(meal);
        }

        // POST: Meals1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var meal = await _context.Meals.SingleOrDefaultAsync(m => m.MealId == id);
            _context.Meals.Remove(meal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MealExists(int id)
        {
            return _context.Meals.Any(e => e.MealId == id);
        }
    }
}
