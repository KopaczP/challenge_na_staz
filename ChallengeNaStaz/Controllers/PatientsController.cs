﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ChallengeNaStaz.Data;
using ChallengeNaStaz.Models;
using Microsoft.AspNetCore.Identity;

namespace ChallengeNaStaz.Controllers
{
    //todo dodać alerty
    //todo dodać zarzadzanie
    //todo poprawić bindowanie
    public class PatientsController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ApplicationDbContext context;

        public PatientsController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.context = context;
        }

        //webapi - uzyte w widoku Patients/Index
        ///todo zabezpieczyć to poprzez token
        public async Task<IActionResult> GetPatients([FromQuery] string queryString)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Patient> patients = new List<Patient>();
            if (!String.IsNullOrEmpty(queryString))
            {
                patients.AddRange(await context.Patients.Where(p => p.Name.Contains(queryString) || p.LastName.Contains(queryString)).ToListAsync());
            }
            else
            {
                patients = await context.Patients.ToListAsync();
            }

            return Ok(patients);
        }

        // GET: Patients
        public async Task<IActionResult> Index()
        {
            var nutritionistID = userManager.GetUserId(HttpContext.User);

            //pokaż pacjentów zalogowanego dietetyka lub admina
            if (nutritionistID != null)
            {
                return View(await context.Patients.Where(p => p.NutritionistId == nutritionistID).ToListAsync());
            }
            return View(new List<Patient>());
        }

        // GET: Patients/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await context.Patients
                .SingleOrDefaultAsync(m => m.PatientId == id);
            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // GET: Patients/Create
        public IActionResult Create()
        {
            return View(new Patient());
        }

        // POST: Patients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PatientId,Name,LastName,DateAdded,DietDuration,DoneConsultations,PlannedWeight,WhatPatientLikes,WhatPatientDoesntLike")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                //przypis twórcy
                var creatorID = userManager.GetUserId(HttpContext.User);
                patient.NutritionistId = creatorID;

                context.Add(patient);
                await context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(patient);
        }

        // GET: Patients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await context.Patients.SingleOrDefaultAsync(m => m.PatientId == id);
            if (patient == null)
            {
                return NotFound();
            }
            return View(patient);
        }

        // POST: Patients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PatientId,Name,LastName,DateAdded,DietDuration,DoneConsultations,PlannedWeight,WhatPatientLikes,WhatPatientDoesntLike")] Patient patient)
        {
            if (id != patient.PatientId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    context.Update(patient);
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientExists(patient.PatientId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(patient);
        }

        // GET: Patients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await context.Patients
                .SingleOrDefaultAsync(m => m.PatientId == id);
            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // POST: Patients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var patient = await context.Patients.SingleOrDefaultAsync(m => m.PatientId == id);
            context.Patients.Remove(patient);
            await context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientExists(int id)
        {
            return context.Patients.Any(e => e.PatientId == id);
        }
    }
}
