﻿using ChallengeNaStaz.Extensions;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Views.Shared.Components
{
    public class Alert : ViewComponent
    {
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<IViewComponentResult> InvokeAsync(Models.Alert alert)
        {
            return View(alert);
        }
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
    }
}
