﻿using ChallengeNaStaz.Data;
using ChallengeNaStaz.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChallengeNaStaz.Views.Shared
{
    [ViewComponent]
    public class MealList : ViewComponent
    {
        private ApplicationDbContext context;

        public MealList (ApplicationDbContext context)
        {
            this.context = context;
        }
        
        ///todo dodać metode publiczną bez parametru ktora zwróci wszystko
        public async Task<IViewComponentResult> InvokeAsync(SearchWithMask query)
        {
            List<Meal> result = new List<Meal>();

            if (String.IsNullOrEmpty(query.QueryString)) //nazwa pusta
            {
                if (query.ExcludeIds != null && query.ExcludeIds.Count > 0) //wyświetl wszystkie oprócz wykluczonych
                {
                    result = await context.Meals.Where(m => !query.ExcludeIds.Contains(m.MealId)).ToListAsync();
                }
                else //wyświetl wszystkie
                {
                    result = await context.Meals.ToListAsync();
                }
            }
            else //nazwa podana
            {
                result = await context.Meals.Where(m => m.MealName.Contains(query.QueryString)).ToListAsync();
                if (query.ExcludeIds != null && query.ExcludeIds.Count > 0) //wyświetl wszystkie z podaną nazwą oprócz wykluczonych
                {
                    result = await context.Meals.Where(m => m.MealName.Contains(query.QueryString) && !query.ExcludeIds.Contains(m.MealId)).ToListAsync();
                }
                else //wyświetl wszystkie z podaną nazwą
                {
                    result = await context.Meals.Where(m => m.MealName.Contains(query.QueryString)).ToListAsync();
                }
            }
            //wszystkie które mają w nazwie cząstke q ale nie są na liście e
            //result = await context.Meals.Where(m => m.MealName.Contains(query.QueryString) && !query.ExcludeIds.Contains(m.MealId)).ToListAsync();
            
            return View(result);
        }
    }
}
