﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ChallengeNaStaz.Migrations
{
    public partial class nav_property : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DietMealDietId",
                table: "Diets",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DietMealMealId",
                table: "Diets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Diets_DietMealDietId_DietMealMealId",
                table: "Diets",
                columns: new[] { "DietMealDietId", "DietMealMealId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Diets_DietMeals_DietMealDietId_DietMealMealId",
                table: "Diets",
                columns: new[] { "DietMealDietId", "DietMealMealId" },
                principalTable: "DietMeals",
                principalColumns: new[] { "DietId", "MealId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Diets_DietMeals_DietMealDietId_DietMealMealId",
                table: "Diets");

            migrationBuilder.DropIndex(
                name: "IX_Diets_DietMealDietId_DietMealMealId",
                table: "Diets");

            migrationBuilder.DropColumn(
                name: "DietMealDietId",
                table: "Diets");

            migrationBuilder.DropColumn(
                name: "DietMealMealId",
                table: "Diets");
        }
    }
}
