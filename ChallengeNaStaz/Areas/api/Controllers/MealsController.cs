﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ChallengeNaStaz.Data;
using ChallengeNaStaz.Models;

namespace ChallengeNaStaz.Areas.api
{
    [Produces("application/json")]
    [Route("api/Meals")]
    public class MealsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MealsController(ApplicationDbContext context)
        {
            _context = context;
        }
        /*
        // GET: api/Meals
        [HttpGet]
        public IEnumerable<Meal> GetMeals()
        {
            return _context.Meals;
        }*/

        //[HttpGet("{queryString}")]
        public async Task<IActionResult> GetMeals([FromQuery] string queryString)
        {
            Console.WriteLine(queryString);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Meal> meals = new List<Meal>();
            if (!String.IsNullOrEmpty(queryString))
            {
                meals = await _context.Meals.Where(m => m.MealName.Contains(queryString)).ToListAsync();
            }
            else
            {
                meals = await _context.Meals.ToListAsync();
            }

            return Ok(meals);
        }

        // GET: api/Meals/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMeal([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var meal = await _context.Meals.SingleOrDefaultAsync(m => m.MealId == id);

            if (meal == null)
            {
                return NotFound();
            }

            return Ok(meal);
        }
        
        // PUT: api/Meals/5
        [HttpGet]
        public async Task<IActionResult> PutMeal([FromQuery] int mealId, [FromQuery] string recipe, [FromQuery] string shoppingList)
        {
            //Meal temp = new Meal { MealId = mealId, Recipe = recipe, ShoppingList = shoppingList };
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Meal meal = _context.Meals.Where(m => m.MealId == mealId).First();
            meal.Recipe = recipe;
            meal.ShoppingList = shoppingList;

            //_context.Entry(temp).State = EntityState.Modified;
            _context.Meals.Update(meal);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MealExists(meal.MealId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Meals
        [HttpPost]
        public async Task<IActionResult> PostMeal([FromBody] Meal meal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Meals.Add(meal);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMeal", new { id = meal.MealId }, meal);
        }

        // DELETE: api/Meals/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMeal([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var meal = await _context.Meals.SingleOrDefaultAsync(m => m.MealId == id);
            if (meal == null)
            {
                return NotFound();
            }

            _context.Meals.Remove(meal);
            await _context.SaveChangesAsync();

            return Ok(meal);
        }

        private bool MealExists(int id)
        {
            return _context.Meals.Any(e => e.MealId == id);
        }
    }
}