﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ChallengeNaStaz.Data;
using ChallengeNaStaz.Models;
using ChallengeNaStaz.Models.UserViewModels;

namespace ChallengeNaStaz.Areas.api.Controllers
{
    [Produces("application/json")]
    [Route("api/Diets")]
    public class DietsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DietsController(ApplicationDbContext context)
        {
            _context = context;
        }
        /*
        // GET: api/Diets
        [HttpGet]
        public IEnumerable<Diet> GetDiets()
        {
            return _context.Diets;
        }*/

        public async Task<IActionResult> GetDiets([FromQuery] string queryString)
        {
            Console.WriteLine(queryString);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Diet> diets = new List<Diet>();
            if (!String.IsNullOrEmpty(queryString))
            {
                diets = await _context.Diets.Where(d => d.DietName.Contains(queryString)).ToListAsync();
            }
            else
            {
                diets = await _context.Diets.ToListAsync();
            }

            return Ok(diets);
        }
        /*
        // GET: api/Diets/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDiet([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var diet = await _context.Diets.SingleOrDefaultAsync(m => m.DietID == id);

            if (diet == null)
            {
                return NotFound();
            }

            return Ok(diet);
        }
        */
        // PUT: api/Diets/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDiet([FromRoute] int id, [FromBody] Diet diet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != diet.DietID)
            {
                return BadRequest();
            }

            _context.Entry(diet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DietExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Diets
        [HttpPost]
        public string PostDiet([FromBody] string dietname, [FromBody] string dietmeals)
        {
            Diet tempDiet = new Diet();
            tempDiet.DietName = dietname;
            tempDiet.DateAdded = DateTime.Now;

            _context.Diets.Add(tempDiet);
            _context.SaveChanges();
            /*
            var idStrings = DietMeals.Split(',');
            List<int> ids = new List<int>();
            foreach (var stringg in idStrings)
            {
                ids.Add(int.Parse(stringg));
            }
            //na ten moment jest gotowa lista id


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            
            await _context.SaveChangesAsync();
            Diet retrievedDiet = _context.Diets.Where(d => d.DietName == DietName).First();
            foreach (int id in ids)
            {
                _context.DietMeals.Add(new DietMeal { DietId = retrievedDiet.DietID, MealId = id });
            }
            await _context.SaveChangesAsync();
            */
            return dietname + dietmeals;
            //return CreatedAtAction("GetDiet", new { id = diet.DietID }, diet);
        }

        // DELETE: api/Diets/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDiet([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var diet = await _context.Diets.SingleOrDefaultAsync(m => m.DietID == id);
            if (diet == null)
            {
                return NotFound();
            }

            _context.Diets.Remove(diet);
            await _context.SaveChangesAsync();

            return Ok(diet);
        }

        private bool DietExists(int id)
        {
            return _context.Diets.Any(e => e.DietID == id);
        }
    }
}